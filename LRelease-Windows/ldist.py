#
# Lightning Python Distribution System
#
# Copyright (C) 2014 Jeffrey Armstrong 
#                    <jeffrey.armstrong@approximatrix.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


# This script will deploy the python distribution to a folder, laid out just like
# reference Python, and then zip up the result or create XML inputs for WiX.

import sys
import os
import os.path
import shutil
import zipfile
import argparse
import xml.dom.minidom
import uuid
from io import StringIO

dest = 'ldist'
dist = 'LPython-{0}.{1}.{2}'.format(sys.version_info.major, 
                                    sys.version_info.minor,
                                    sys.version_info.micro)

winbuild_directory = 'OWbuild-Windows'

necessary_dlls = [os.path.join(winbuild_directory, 'sqlite3', 'sqlite3.dll'), ]

def copy_files_of_type(srcdir, destdir, ext):
    """Copies all files of a given type in a source folder into a
    destination folder.  Extension should probably include a dot."""
    
    for x in os.listdir(srcdir):
        x_base, x_ext = os.path.splitext(x)
        if x_ext == ext:
            shutil.copy(os.path.join(srcdir, x), destdir)

def deploy(skip_stdlib=False, skip_dev=False, skip_tools=False):
    
    fullpath = os.path.join(dest, dist)
    
    if not os.path.exists(dest):
        os.mkdir(dest)
    if not os.path.exists(fullpath):
        os.mkdir(fullpath)
    
    # Executable    
    print("Deploying Interpretter")
    shutil.copy(os.path.join(winbuild_directory, 'python.exe'),
                fullpath)
    shutil.copy(os.path.join(winbuild_directory, 
                             'python{0}{1}.dll'.format(sys.version_info.major, 
                                                       sys.version_info.minor)),
                             fullpath)
    
    # DLLs and PYDs
    print("Deploying Dynamic Libs")
    dll_dir = os.path.join(fullpath, 'DLLs')
    if not os.path.exists(dll_dir):
        os.mkdir(dll_dir)
    copy_files_of_type(winbuild_directory, dll_dir, ".pyd")
    
    # Simple docs
    print("Copying Simple Docs")
    shutil.copy('LICENSE', os.path.join(fullpath, 'LICENSE.txt'))
    shutil.copy('README', os.path.join(fullpath, 'README.original.txt'))
    shutil.copy('README.lightning', os.path.join(fullpath, 'README.lightning.txt'))
    
    # Import libraries and headers
    if skip_dev:
        print('Skipping development files')
    else:
        print("Deploying Development Files")
        lib_dir = os.path.join(fullpath, 'libs')
        if not os.path.exists(lib_dir):
            os.mkdir(lib_dir)
        copy_files_of_type(winbuild_directory, lib_dir, ".lib")
        h_dir = os.path.join(fullpath, 'include')
        if not os.path.exists(h_dir):
            os.mkdir(h_dir)
        copy_files_of_type('Include', h_dir, ".h")
        shutil.copy(os.path.join('PC', 'pyconfig.h'), h_dir)
        
    # Create an empty scripts directory
    print("Generating Scripts Directory")
    scr_dir = os.path.join(fullpath, 'Scripts')
    if not os.path.exists(scr_dir):
        os.mkdir(scr_dir)
        
        
    cache_ignore = shutil.ignore_patterns("__pycache__", "*.pyc")
    
    # Deploy Tools
    if skip_tools:
        print('Skipping Tool Deployment')
    else:
        print("Deploying Some Tools")
        tools_dir = os.path.join(fullpath, 'Tools')
        if not os.path.exists(tools_dir):
            os.mkdir(tools_dir)
            
        toolscr_dir = os.path.join(tools_dir, 'Scripts')
        if os.path.exists(toolscr_dir):
            shutil.rmtree(toolscr_dir)
        shutil.copytree(os.path.join('Tools', 'Scripts'), toolscr_dir, ignore=cache_ignore)
        
    # And the standard lib
    if skip_stdlib:
        print('Skipping the Standard Library')
    else:
        print("Deploying the Python Standard Library")
        stdlib_dir = os.path.join(fullpath, 'Lib')
        if os.path.exists(stdlib_dir):
            shutil.rmtree(stdlib_dir)
        shutil.copytree('Lib', stdlib_dir, ignore=cache_ignore)
        
    # And misc DLLs
    print("Deploying Miscellaneous DLLs")
    for f in necessary_dlls:
        dll = os.path.basename(f)
        shutil.copy(f, os.path.join(dll_dir, dll))
        
    return fullpath
        
def zipit(fullpath):

    # And zipping...
    print("Zipping...")
    if os.path.exists(fullpath+'.zip'):
        os.remove(fullpath+'.zip')
        
    zf = zipfile.ZipFile(fullpath+'.zip', 'w', compression=zipfile.ZIP_DEFLATED)
    for root, dirs, files in os.walk(fullpath):
        for f in files:
            diskname = os.path.join(root, f)
            zipname = os.path.relpath(diskname, dest)
            zf.write(diskname, arcname=zipname)
    zf.close()
    
    print("Distribution is located at {0}".format(fullpath+'.zip'))

def gen_wix_id():
    return str(uuid.uuid1()).replace("-", "")

def construct_msi_strings(distdir, workdir):
    
    doc = xml.dom.minidom.Document()

    # We'll now construct

    dir = []
    dir.append((distdir, doc))
    
    topElement = doc.createElement("Root")
    doc.appendChild(topElement)
    dir.append((distdir, topElement))

    # need to return a component id list
    components = []
    
    for root, dirs, files in os.walk(distdir, topdown=True):

        # Remove tests
        if 'test' in dirs:
            dirs.remove('test')

        # Create a basic path to our source
        start = root.replace(workdir+os.sep,'')
        parent, thisdir = os.path.split(root)
        while parent != dir[-1][0] and dir[-1][1] != topElement:
            dir.pop()
        
        # Create a Directory node
        thisnode = doc.createElement("Directory")
        thisnode.setAttribute("Name", thisdir)
        did = gen_wix_id()
        
        thisnode.setAttribute("Id", did)
        dir[-1][1].appendChild(thisnode)
        dir.append((root, thisnode))
        
        # Create a component for the files in said directory
        cnode = doc.createElement('Component')
        cnode.setAttribute('Id', gen_wix_id())
        cnode.setAttribute("DiskId", "1")
        cnode.setAttribute("Guid", str(uuid.uuid1()))

        # If there are files, append the component node
        if files is not None and len(files) > 0:        
            thisnode.appendChild(cnode)
            components.append(cnode)
        
        # Add each file to the component (not recommended, but meh...)
        for name in files: 
            fnode = doc.createElement('File')
            fnode.setAttribute("Name", name)
            fnode.setAttribute("Source", os.path.join(start, name))
            
            if name == 'python.exe':
                fnode.setAttribute("Id", "python")
            else:
                fnode.setAttribute("Id", gen_wix_id())
                
            cnode.appendChild(fnode)
            
    # Create an XML fragment
    filesxml = topElement.toprettyxml(indent='    ')
    filesxml = filesxml.replace("<Root>", "").replace("</Root>","")
    
    # Build a list of ComponentRefs, but don't bother using a DOM
    componentsSIO = StringIO()
    for x in components:
        componentsSIO.write('                <ComponentRef Id="{0}" />\n'.format(x.getAttribute("Id")))
    componentRefs = componentsSIO.getvalue()
    componentsSIO.close()
    
    return filesxml, componentRefs

def msi(fullpath):
    
    # The working dir is where our wxs file will be written
    working_dir = os.path.dirname(fullpath)
    
    # First build the XML components
    print("Constructing WiX Components XML")
    files, refs = construct_msi_strings(fullpath, working_dir)
    
    # Read in the WiX template
    template = ""
    with open(os.path.join(os.path.dirname(__file__), 'lpython.wxs'), "r") as f:
        template = f.read()
        
    # Write out the WiX file to the parent directory of the distro
    print("Generating WiX Input File")
    with open(os.path.join(working_dir, 'lpython.wxs'), "w") as f:
        f.write(template.format(major=sys.version_info.major, 
                                minor=sys.version_info.minor,
                                micro=sys.version_info.micro,
                                distfiles=files,
                                distcomponents=refs))

def main(cmds, skip_stdlib=False, skip_dev=False, skip_tools=False):
    
    # Always deploy...
    dist_path = deploy(skip_stdlib, skip_dev, skip_tools)
    
    for cmd in cmds:
        if cmd == 'zip':
            zipit(dist_path)
        elif cmd == 'msi':
            msi(dist_path)
        else:
            print("No additional work specified")

if __name__ == '__main__':

    parser = argparse.ArgumentParser(description='Create a Lightning Python distributable zip')

    parser.add_argument('command', metavar='cmd', type=str, nargs='*',
                        help='command for this script, either "zip" or "msi"')

    parser.add_argument('--skip-stdlib', dest='skip_stdlib', 
                        action='store_true', default=False,
                        help='do not copy the stdlib (Lib) directory')

    parser.add_argument('--skip-dev', dest='skip_dev', 
                        action='store_true', default=False,
                        help='do not copy the development files')

    parser.add_argument('--skip-tools', dest='skip_tools', 
                        action='store_true', default=False,
                        help='do not copy the tools (Tools) directory')

    args = parser.parse_args()
    
    main(args.command,
         skip_stdlib=args.skip_stdlib,
         skip_dev=args.skip_dev,
         skip_tools=args.skip_tools)
