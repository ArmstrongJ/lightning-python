Lightning Python Distribution System
====================================

The Lightning Python Distribution System is a simple Python script
and XML template for preparing distributions of Lightning Python.
It generally provides two functions:

1. Create a zip file containing the distribution
2. Create the necessary inputs for use with WiX, the
   Windows Installer XML toolset
   
This script system is used basically because the MSI-building
scripts included with Python (Tools/msi) are entirely broken. 
The Python tool is only compatible with Python 2.x, and it jumps
through hoops to provide upgrade compatibility.  Fixing CPython's
scripts, though, is futile as the core devs are toying with the
idea of switching to WiX (or at least the WiX Visual Studio
plugins) anyway.

Executing
---------

The distribution script, ldist.py, must be run by the Lightning
Python interpreter as it relies on the interpreter for version
information while constructing the necessary filenames.  The
script accepts a handful of options and two commands: 'zip' and
'msi'.  Using the '--help' option will provide more information.

License
-------

The Lightning Python Distribution System, including the Python script
and WiX XML template are licensed under the GNU General Public License
Version 3.  Please note that this licensing is _not_ the same as 
Python itself.

Lightning Python Distribution System
Copyright (C) 2014 Jeffrey Armstrong 
                   <jeffrey.armstrong@approximatrix.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.