Welcome To Lighning Python
==========================

Lightning Python is a project seeking to provide an implementation of
CPython (the reference implementation) compiled using the Open Watcom
compiler.  Originally meant to provide a port of Python for Windows 
compiled with Open Watcom, the project is being extended to more
generally encompass compiling Python using the Open Watcom toolchain
regardless of platform.

As this port is "in progress," a number of standard library modules may
not be implemented if they use C extensions.  The specifics are
covered for each platofm below.

The name "Lightning" just references the logo for Open Watcom.  It does
not imply Lightning Python is faster than stock Python.  In fact, it is
considerably slower at this moment on Windows.

Windows
-------

In general terms, Lightning Python for Windows is complete with a
few exceptions.  At the moment, SQLite, hashlib, and tkinter are all 
absent; these missing modules will be implemented in the future.  
Furthermore, there may be bugs in the interpretter itself.  The 
standard library's regression test suite fails or outright crashes in a
number of locations.  Crashes are most likely due to a problems with
cffi.

Python 3.4 is currently supported on Windows.

Linux
-----

The Linux port is actually relatively straightforward and functional.
However, Open Watcom lacks support for shared objects under Linux.  Due
to this missing feature, the interpreter must be statically linked, and
a number of native modules are currently not included in the build.

Python 3.5 is currently supported on Linux.

Building
--------

To build Python, you'll first need to acquire Open Watcom.  The
unofficial version 2.0 fork is necessary as it implements a number of
modern Windows API and C runtime calls that are not available in the
last official release (from over two years ago).

The builds of the Open Watcom compiler can be downloaded from links on
the project's GitHub page:

  https://github.com/open-watcom/open-watcom-v2
  
Alternatively, nightly binaries are available as simple archives from:

  http://buildbot.approximatrix.com/
  
Once Open Watcom is installed, you'll need to acquire the source to 
Lightning Python.  

### Windows

After downloading the source, the project "python.wpj" in the
"OWbuild-Windows" directory should be opened using the Open Watcom 
IDE.  The target "python34.dll" should be built first as most 
everything else relies on it.  Next, it is suggested that the 
"python.exe" target should be built.  Finally, the remaining DLL 
projects, which are subsequently copied into the appropriate pyd 
files, can be built.

The Windows port also includes a distribution system for generating a
true MSI installer in the LRelease-Windows directory.

### Linux

After downloading an appropriate source distribution, you'll need to
first apply the needed patch from Patches/.   Next, copy the build 
script from OWbuild-Linux/ to the root of the Python source tree.
Finally, execute build.sh and wait a minute or two for the interpreter
to be built.  More details are at OWbuild-Linux/README.md.

Compatibility
-------------

Lightning Python is generally compatible with stock Python, excpeting
missing native modules as pointed out earlier.

Contact
-------

Lightning Python is hosted on bitbucket, where you can find the bug
tracker, a wiki, and downloads:

    https://bitbucket.org/ArmstrongJ/lightning-python

The current lead, Jeff Armstrong, is available at:

    Email:   jeff@rainbow-100.com
    Twitter: @fortranjeff
    

