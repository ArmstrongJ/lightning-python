#
# Lightning Python - Simple Dist Script
# 
# This script will deploy the python distribution to a folder, laid out just like
# reference Python, and then zip up the result.

import sys
import os
import os.path
import shutil
import zipfile
import argparse

dest = 'ldist'
dist = 'LPython-{0}.{1}.{2}'.format(sys.version_info.major, 
                                    sys.version_info.minor,
                                    sys.version_info.micro)

winbuild_directory = 'OWbuild-Windows'

def copy_files_of_type(srcdir, destdir, ext):
    """Copies all files of a given type in a source folder into a
    destination folder.  Extension should probably include a dot."""
    
    for x in os.listdir(srcdir):
        x_base, x_ext = os.path.splitext(x)
        if x_ext == ext:
            shutil.copy(os.path.join(srcdir, x), destdir)

def main(skip_stdlib=False, skip_dev=False, skip_tools=False):
    
    fullpath = os.path.join(dest, dist)
    
    if not os.path.exists(dest):
        os.mkdir(dest)
    if not os.path.exists(fullpath):
        os.mkdir(fullpath)
    
    # Executable    
    print("Deploying Interpretter")
    shutil.copy(os.path.join(winbuild_directory, 'python.exe'),
                fullpath)
    shutil.copy(os.path.join(winbuild_directory, 
                             'python{0}{1}.dll'.format(sys.version_info.major, 
                                                       sys.version_info.minor)),
                             fullpath)
    
    # DLLs and PYDs
    print("Deploying Dynamic Libs")
    dll_dir = os.path.join(fullpath, 'DLLs')
    if not os.path.exists(dll_dir):
        os.mkdir(dll_dir)
    copy_files_of_type(winbuild_directory, dll_dir, ".pyd")
    
    # Simple docs
    print("Copying Simple Docs")
    shutil.copy('LICENSE', os.path.join(fullpath, 'LICENSE.txt'))
    shutil.copy('README', os.path.join(fullpath, 'README.original.txt'))
    shutil.copy('README.lightning', os.path.join(fullpath, 'README.lightning.txt'))
    
    # Import libraries and headers
    if skip_dev:
        print('Skipping development files')
    else:
        print("Deploying Development Files")
        lib_dir = os.path.join(fullpath, 'libs')
        if not os.path.exists(lib_dir):
            os.mkdir(lib_dir)
        copy_files_of_type(winbuild_directory, lib_dir, ".lib")
        h_dir = os.path.join(fullpath, 'include')
        if not os.path.exists(h_dir):
            os.mkdir(h_dir)
        copy_files_of_type('Include', h_dir, ".h")
        shutil.copy(os.path.join('PC', 'pyconfig.h'), h_dir)
        
    # Create an empty scripts directory
    print("Generating Scripts Directory")
    scr_dir = os.path.join(fullpath, 'Scripts')
    if not os.path.exists(scr_dir):
        os.mkdir(scr_dir)
        
        
    cache_ignore = shutil.ignore_patterns("__pycache__", "*.pyc")
    
    # Deploy Tools
    if skip_tools:
        print('Skipping Tool Deployment')
    else:
        print("Deploying Some Tools")
        tools_dir = os.path.join(fullpath, 'Tools')
        if not os.path.exists(tools_dir):
            os.mkdir(tools_dir)
            
        toolscr_dir = os.path.join(tools_dir, 'Scripts')
        if os.path.exists(toolscr_dir):
            shutil.rmtree(toolscr_dir)
        shutil.copytree(os.path.join('Tools', 'Scripts'), toolscr_dir, ignore=cache_ignore)
        
    # And the standard lib
    if skip_stdlib:
        print('Skipping the Standard Library')
    else:
        print("Deploying the Python Standard Library")
        stdlib_dir = os.path.join(fullpath, 'Lib')
        if os.path.exists(stdlib_dir):
            shutil.rmtree(stdlib_dir)
        shutil.copytree('Lib', stdlib_dir, ignore=cache_ignore)
        
    # And zipping...
    print("Zipping...")
    if os.path.exists(fullpath+'.zip'):
        os.remove(fullpath+'.zip')
        
    zf = zipfile.ZipFile(fullpath+'.zip', 'w', compression=zipfile.ZIP_DEFLATED)
    for root, dirs, files in os.walk(fullpath):
        for f in files:
            diskname = os.path.join(root, f)
            zipname = os.path.relpath(diskname, dest)
            zf.write(diskname, arcname=zipname)
    zf.close()
    
    print("Distribution is located at {0}".format(fullpath+'.zip'))

if __name__ == '__main__':

    parser = argparse.ArgumentParser(description='Create a Lightning Python distributable zip')

    parser.add_argument('--skip-stdlib', dest='skip_stdlib', 
                        action='store_true', default=False,
                        help='do not copy the stdlib (Lib) directory')

    parser.add_argument('--skip-dev', dest='skip_dev', 
                        action='store_true', default=False,
                        help='do not copy the development files')

    parser.add_argument('--skip-tools', dest='skip_tools', 
                        action='store_true', default=False,
                        help='do not copy the tools (Tools) directory')

    args = parser.parse_args()
    main(skip_stdlib=args.skip_stdlib,
         skip_dev=args.skip_dev,
         skip_tools=args.skip_tools)
