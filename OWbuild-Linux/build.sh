#!/bin/sh

# Lightning Python Linux build script
#
# To run this script, copy it first to the root directory of the
# Python source directory.  Next, be sure to apply the appropriate
# patch from the Patches/ directory.  Finally, run this script and
# wait.  About a minute or two later, you should have a functioning
# Python interpretter.

# Force wlib into AR-compatible mode
WLIB_AR=true

if ! command -v owcc >/dev/null 2>&1
then
	echo "Open Watcom's Compiler Driver (owcc) is not on the path"
	echo "Please correct this issue and try again"
	exit 1
fi

# Command-line parsing
TERM_DATADIR=/lib
INSTALL_PREFIX=/opt/lpython
for i in "$@"
do
case $i in
    -p=*|--prefix=*)
    INSTALL_PREFIX="${i#*=}"
    ;;
    -d=*|--datadir=*)
    TERM_DATADIR="${i#*=}"
    ;;
esac
done


### zlib Processing
ZLIB_VERSION=1.2.11
if [ ! -e "Modules/zlib-$ZLIB_VERSION/libz.a" ]
then
    wget -O/tmp/zlib.tar.gz "http://zlib.net/zlib-$ZLIB_VERSION.tar.gz"
    tar -xzvC Modules -f /tmp/zlib.tar.gz
    cd Modules/zlib-$ZLIB_VERSION
    CC=owcc CFLAGS="-za99 -O3 -mtune=i6 -mthreads" AR=wlib ARFLAGS=" " ./configure --static
    make libz.a
    cd ../..
fi


### NCurses Processing
NCURSES_VERSION=6.1
if [ ! -e "Modules/libncurses.a" ]
then
    wget -O/tmp/ncurses.tar.gz "ftp://ftp.gnu.org/gnu/ncurses/ncurses-$NCURSES_VERSION.tar.gz"
    tar -xzC Modules -f /tmp/ncurses.tar.gz
    cd Modules/ncurses-$NCURSES_VERSION
    ./configure --prefix=/usr --datarootdir=$TERM_DATADIR --without-cxx --without-cxx-binding CC=owcc AR=wlib ARFLAGS=" " RANLIB="echo"
    make libs
    cp lib/libncurses.a ..
    cd ../..
fi


### Readline Processing
READLINE_VERSION=7.0
if [ ! -e "Modules/libreadline.a" ]
then
    wget -O/tmp/readline.tar.gz "ftp://ftp.gnu.org/gnu/readline/readline-$READLINE_VERSION.tar.gz"
    tar -xzC Modules -f /tmp/readline.tar.gz
    cd Modules/readline-$READLINE_VERSION
    ./configure --enable-static --disable-shared CC=owcc CFLAGS="-za99 -O3 -mtune=i6 -mthreads" AR=wlib ARFLAGS=" " 
    
    # Fix backspace for whatever reason
    sed -i.bak "s/SET_SPECIAL (VKILL, rl_unix_line_discard);/SET_SPECIAL (VKILL, rl_rubout);/g" rltty.c
    
    make all
    cp *.a ..
    cd ../..
    # For proper includes
    ln -s Modules/readline-$READLINE_VERSION readline
fi

### Configure if Makefile doesn't yet exist
if [ ! -e "Makefile" ]
then
    ./configure --prefix=$INSTALL_PREFIX --disable-shared \
                --with-libs="-lModules/zlib-$ZLIB_VERSION/libz -lModules/libreadline -lModules/libncurses" \
                CC=owcc CFLAGS="-za99 -O3 -mtune=i6 -mthreads -fnonconst-initializers -DXML_DEV_URANDOM" \
                AR=wlib ARFLAGS="-r -c" \
                LDFLAGS="-mthreads" \
                LINKFORSHARED=" " 
fi

### Backup the modules setup configuration
if [ ! -e "Modules/Setup.original" ]
then
    cp Modules/Setup Modules/Setup.original
else
    cp Modules/Setup.original Modules/Setup
fi

### SQLite Processing
SQLITE_VERSION=3180000
if [ ! -e "Modules/_sqlite/sqlite.zip" ]
then
    echo "Acquiring SQLite"
    wget -OModules/_sqlite/sqlite.zip http://sqlite.org/2017/sqlite-amalgamation-$SQLITE_VERSION.zip
    unzip -d Modules/_sqlite Modules/_sqlite/sqlite.zip
fi
if [ ! -e "Modules/_sqlite/sqlite3.c" ]
then
    cp Modules/_sqlite/sqlite-amalgamation-$SQLITE_VERSION/sqlite3.c Modules/_sqlite/sqlite3.c
    sed -i '1s;^;#define SQLITE_OMIT_LOAD_EXTENSION 1\n#define HAVE_UTIME 1\n;' Modules/_sqlite/sqlite3.c
fi
if [ ! -e "Modules/_sqlite/sqlite3.h" ]
then
    cp Modules/_sqlite/sqlite-amalgamation-$SQLITE_VERSION/sqlite3.h Modules/_sqlite/sqlite3.h
    sed -i '1s;^;#define SQLITE_OMIT_LOAD_EXTENSION 1\n;' Modules/_sqlite/sqlite3.h
fi

echo "Fixing SQLite for this build system"
for sf in cache.c connection.c cursor.c microprotocols.c module.c prepare_protocol.c row.c statement.c util.c; do
    sed -i.bak "s/MODULE_NAME/\"sqlite\"/g" Modules/_sqlite/$sf
done

# Need to enable a bunch of modules to be statically linked
# to our Python executable since Open Watcom doesn't support
# shared libs on Linux
echo "Updating static module list"
echo "# Manual additions for lightning-python" >> Modules/Setup
echo "_asyncio _asynciomodule.c" >> Modules/Setup
echo "_opcode _opcode.c" >> Modules/Setup
echo "_multiprocessing _multiprocessing/multiprocessing.c" >> Modules/Setup
echo "_sha3 _sha3/sha3module.c" >> Modules/Setup
echo "_sqlite3 _sqlite/cache.c _sqlite/connection.c _sqlite/cursor.c _sqlite/microprotocols.c _sqlite/module.c _sqlite/prepare_protocol.c _sqlite/row.c _sqlite/statement.c _sqlite/util.c _sqlite/sqlite3.c" >> Modules/Setup
echo "zlib zlibmodule.c -IModules/zlib-$ZLIB_VERSION" >> Modules/Setup
echo "zlib zlibmodule.c -IModules/zlib-$ZLIB_VERSION" >> Modules/Setup
echo "readline readline.c" >> Modules/Setup
echo "# End lightning-python" >> Modules/Setup
for mod in pyexpat cmath math _struct time _random _pickle _datetime select _posixsubprocess array _weakref _bisect _heapq unicodedata _csv _md5 _socket mmap binascii parser _sha1 _sha256 _sha512 _blake2 atexit termios fcntl elementtree; do
    echo "  enabling $mod"
    sed -i "s/#$mod/$mod/g" Modules/Setup
done

# Make the Makefile again
make Makefile

# Change flags and executables in the Makefile
echo "Removing ranlib from Makefile"
sed -i 's/ranlib/echo/g' Makefile

echo "Changing library extensions"
sed -i 's/\.a$/.lib/g' Makefile

# Have Python report Open Watcom as our compiler
echo "Inserting compiler details"
if [ ! -e "Python/getcompiler.c.original" ]
then
    cp Python/getcompiler.c Python/getcompiler.c.original
else
    cp Python/getcompiler.c.original Python/getcompiler.c
fi
sed -i 's/return COMPILER;/return "[Open Watcom]";/g' Python/getcompiler.c

# Python's configure script makes a good deal of mistakes
echo "Correcting configure mistakes"
sed -i 's/#define HAVE_DEV_PTMX 1/\/* #undef HAVE_DEV_PTMX *\//g' pyconfig.h
#sed -i 's/#define HAVE_SCHED_H 1/\/* #undef HAVE_SCHED_H *\//g' pyconfig.h
sed -i 's/#define HAVE_SYS_RESOURCE_H 1/\/* #undef HAVE_SYS_RESOURCE_H *\//g' pyconfig.h

# Python core devs refuse to address this bug...
echo "Correcting core developers' ignorance of bug 23876"
sed -i 's/__QNX__/__UNIX__/g' Modules/posixmodule.c
sed -i 's/__QNX__/__UNIX__/g' Modules/timemodule.c

# And this bug...
echo "Correcting use of _asctime"
sed -i 's/_asctime/_pyasctime/g' Modules/timemodule.c

# Poor choice of macros...
echo "Changing macro to function"
sed -i 's/PyBytes_AS_STRING/PyBytes_AsString/g' Modules/_datetimemodule.c

# Build the interpreter
make python

# Build the _sysconfigdata module - needed to start the compiler
make platform

# The interpreter can now be run in-place
