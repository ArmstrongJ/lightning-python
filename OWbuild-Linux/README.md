Lightning Python Linux Build Script
===================================

In recent times, additions to the Open Watcom standard library for
Linux has enabled compiling the Python interpreter with the Open
Watcom toolchain for Linux systems.  While not necessarily complete,
Lightning Python on Linux does actually work, and users can execute
the Tools/pybench benchmarks successfully in its current state.

The main shortcoming of using Lightning Python on Linux at this
time is Open Watcom's lack of shared object support on Linux.  There
is some talk of adding shared objects, but there is no definitive
timeframe for the project to be completed.

Requirements
------------

Building Lightning Python requires a recent version of the Open
Watcom version 2 fork.  You can download the Open Watcom source
from:

https://github.com/open-watcom/open-watcom-v2

If compiling Open Watcom is too much for you (it's extremely
complicated), there are binaries available as installers from the
site above or zip files available from:

http://buildbot.approximatrix.com/

The Open Watcom toolchain must be on the shell's path prior to
attempting to use the build script.

Building
--------

Generally speaking, compiling Python requires three steps:

  1. Patching
  2. Configuring
  3. Building
  
Patches are necessary for Lightning Python on Linux, but they are
generally quite limited thanks to Open Watcom's reasonable POSIX
compliance.  Most of the patching is meant to correct issues with
the source code assuming the Open Watcom compiler would only be
used on Windows, DOS, or QNX.  Patches are available in the 
../Patches directory.

Configuring can be done by hand if the user specifies the Open
Watcom POSIX compiler wrapper as the C compiler of choice.  With a few
additional changes, "make python" should just work.  To simplify
matters, we've provided a build script, build.sh, that properly
executes GNU autoconf tools, enables some desired modules (since they
have to be statically linked), corrects a few configure mistakes, and
compiles the interpreter into a working executable.  We strongly
suggest simply using the included script.

To run the script, first apply necessary patches.  Next, copy the
script to the root directory of the Python source tree.  Finally,
run the script and wait.  Open Watcom is blazing fast, so you should
have a functional interpreter in about a minute.